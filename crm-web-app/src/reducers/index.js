import { combineReducers } from "redux";
import customersReducer from "./customersReducer";
import errorsReducer from "./errorsReducer";
import apiStatusReducer from "./apiStatusReducer";

const rootReducer = combineReducers({
  customers: customersReducer,
  apiStatus: apiStatusReducer,
  errors: errorsReducer,
});

export default rootReducer;
