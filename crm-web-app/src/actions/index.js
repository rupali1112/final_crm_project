import axios from "axios";

// for our Action Creators
export const fetchCustomersBegin = () => {
  return {
    type: "FETCH_CUSTOMERS_BEGIN",
  };
};

export const fetchCustomersSuccess = (customers) => {
  return {
    type: "FETCH_CUSTOMERS_SUCCESS",
    payload: customers,
  };
};

export const fetchCustomersFailure = (err) => {
  return {
    type: "FETCH_CUSTOMERS_FAILURE",
    payload: { message: "Failed to fetch customers.. please try again later" },
  };
};

export const getApiStatusBegin = () => {
  return {
    type: "GET_APISTAUS_BEGIN",
  };
};

export const getApiStatusSuccess = (status) => {
  return {
    type: "GET_APISTATUS_SUCCESS",
    payload: status,
  };
};

export const getApiStatusFailure = (err) => {
  return {
    type: "GET_APISTATUS_FAILURE",
    payload: { message: "Failed to get staus.. please try again later" },
  };
};

// to be call by the components
export const fetchCustomers = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchCustomersBegin());
    console.log("state after fetchCustomersBegin", getState());
    axios.get("http://localhost:8080/api/payments/findAll").then(
      (res) => {
        setTimeout(() => {
          dispatch(fetchCustomersSuccess(res.data));
          console.log("state after fetchCustomersSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_CUSTOMERS_FAILURE
        dispatch(fetchCustomersFailure(err));
        console.log("state after fetchCustomersFailure", getState());
      }
    );
  };
};

export const addCustomerBegin = () => {
  return {
    type: "ADD_CUSTOMER_BEGIN",
  };
};

export const addCustomerSuccess = () => {
  return {
    type: "ADD_CUSTOMER_SUCCESS",
  };
};

export const addCustomerFailure = (err) => {
  return {
    type: "ADD_CUSTOMER_FAILURE",
    payload: { message: "Failed to add new customer.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addCustomer = (customer) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/api/payments/save", customer).then(
      () => {
        console.log("customer created!");
        // this is where we can dispatch ADD_CUSTOMER_SUCCESS
        dispatch(addCustomerSuccess());
      },
      (err) => {
        dispatch(addCustomerFailure(err));
        console.log("state after addCustomerFailure", getState());
      }
    );
  };
};

export const getApiStatus = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(getApiStatusBegin());
    console.log("state after getApiStatusBegin", getState());
    axios.get("http://localhost:8080/api/payments/apiStatus").then(
      (res) => {
        setTimeout(() => {
          dispatch(getApiStatusSuccess(res.data));
          console.log(res.data);
          console.log("state after getApiStatusSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch GET_APISTATUS_FAILURE
        dispatch(getApiStatusFailure(err));
        console.log("state after getApiStatusFailure", getState());
      }
    );
  };
};
