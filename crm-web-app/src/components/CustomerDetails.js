import React from "react";

const CustomerDetails = ({
  customerId,
  name,
  age,
  email,
  address,
  phone,
  visible,
}) => {
  return (
    visible && (
      <ol className="album-tracks">
        <li key={customerId}>ID: {customerId}</li>
        <li key={name}>Name: {name}</li>
        <li key={age}>Age: {age}</li>
        <li key={email}>Email: {email}</li>
        <li key={address}>Address: {address}</li>
        <li key={phone}>Phone: {phone}</li>
      </ol>
    )
  );
};

export default CustomerDetails;
