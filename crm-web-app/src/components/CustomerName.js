import React from "react";

const CustomerName = ({ name }) => (
  <div>
    <h3 className="album-artist card-subtitle mb-2 text-muted">Name: {name}</h3>
  </div>
);

export default CustomerName;
