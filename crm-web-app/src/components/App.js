import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Banner from "./Banner";
import Navbar from "./Navbar";
import CustomersListing from "./CustomersListing";
import CreateCustomerForm from "./CreateCustomerForm";
import { fetchCustomers, getApiStatus } from "../actions"; // pick up index.js by default
import CustomerApiStatus from "./CustomerApiStatus";

const App = () => {
  const dispatch = useDispatch();
  const [fetchCustomer, setFetchCustomer] = useState(false);
  const [getStatus, setGetStatus] = useState(false);

  useEffect(() => {
    dispatch(fetchCustomers()); // dispatch fetchCustomers action
  }, [fetchCustomer, fetchCustomers]); // run this at start and whenever fetchCustomer is changed

  useEffect(() => {
    dispatch(getApiStatus()); // dispatch getApiStatus action
  }, [getStatus, getApiStatus]); // run this at start and whenever getApiStatus is changed

  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Banner />
            <CustomersListing />
          </Route>
          <Route path="/add">
            <CreateCustomerForm
              fetchCustomer={fetchCustomer}
              setFetchCustomer={setFetchCustomer}
            />
          </Route>
          <Route path="/about">
            <CustomerApiStatus />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
