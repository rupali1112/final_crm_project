import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => (
  <ul className="nav">
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/"
        activeClassName="app"
      >
        Home
      </NavLink>
    </li>
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/about"
        activeClassName="app"
      >
        About
      </NavLink>
    </li>
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/add"
        activeClassName="app"
      >
        Add Customer
      </NavLink>
    </li>
  </ul>
);

export default Navbar;
