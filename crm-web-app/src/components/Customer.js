import React, { useState } from "react";
import CustomerName from "./CustomerName";
import CustomerDetails from "./CustomerDetails";
import ShowHideButton from "./ShowHideButton";

const Customer = ({ customerId, name, age, email, address, phone }) => {
  const [visible, setVisibity] = useState(true);

  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body">
          <CustomerName name={name} />
          <CustomerDetails
            customerId={customerId}
            age={age}
            email={email}
            address={address}
            phone={phone}
            visible={visible}
          />
          <ShowHideButton toggle={setVisibity} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default Customer;
