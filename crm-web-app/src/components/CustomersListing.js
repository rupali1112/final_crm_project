import React from "react";
import Customer from "./Customer";
import { useSelector } from "react-redux";

const CustomersListing = () => {
  const customers = useSelector((state) => state.customers.entities);
  const error = useSelector((state) => state.customers.error);
  const loading = useSelector((state) => state.customers.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <div className="row">
        {customers.map((customer) => (
          <Customer
            customerId={customer.customerId}
            name={customer.name}
            age={customer.age}
            email={customer.email}
            address={customer.address}
            phone={customer.phone}
          />
        ))}
      </div>
    </div>
  );
};

export default CustomersListing;
