import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addCustomer } from "../actions";

const CreateCustomerForm = ({ fetchCustomer, setFetchCustomer }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [customerId, setCustomerId] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // dispatch addCustomer action
    dispatch(
      addCustomer({
        customerId: customerId,
        name: name,
        age: age,
        email: email,
        address: address,
        phone: phone,
      })
    )
      .then(() => {
        console.log("addCustomer is successful");
        setFetchCustomer(!fetchCustomer);
      })
      .catch(() => {})
      .finally(() => {
        console.log("addCustomer thunk function is completed");
        history.push("/");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add New Customer</h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="customerId">Customer ID:</label>
            <input
              id="customerId"
              type="number"
              className="form-control"
              value={customerId}
              onChange={(e) => setCustomerId(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="name">Name:</label>
            <input
              id="name"
              type="text"
              className="form-control"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="age">Age:</label>
            <input
              id="type"
              type="number"
              className="form-control"
              value={age}
              onChange={(e) => setAge(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="email">Email:</label>
            <input
              id="email"
              type="text"
              className="form-control"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="address">Address:</label>
            <input
              id="address"
              type="text"
              className="form-control"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
        </div>
        <div className="form-group">
          <input type="submit" value="Add Customer" />
        </div>
      </form>
    </div>
  );
};

export default CreateCustomerForm;
